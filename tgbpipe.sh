#!/usr/bin/env bash

VERSION="0.1.1"

if [[ -r /etc/tgbpipe.conf ]] #Read the config files
then
    source /etc/tgbpipe.conf
fi
if [[ -r "$HOME/.config/tgbpipe.conf" ]]
then
    source "$HOME/.config/tgbpipe.conf"
fi

while [[ $# -gt 0 ]] #Loop through all arguments
do

    case $1 in
	-h|-\?|--help) #Display help
	    echo -e "Usage: tgbpipe [OPTION]\nTelegram bot pipe. Send a text to a user via telegram bot\n\n -c, --config file\tRead config from file. If unspecified, read from system config\n\n -t, --text file\tSend text from file. If unspecified, read from stdin\n\n -k, --token token\tBot token to use\n\n -u, --uid userid\tUser ID to send to. Multiple recipient IDs in future versions \n\n -h, -?, --help\t\tShow this help\n\n -v, --version\t\tShow version\n\nIf multiple instances of -c, -k, -t and -u are specified, the last one in the\ncommand line is used.\n\nConfig files are read with source and should countain two variable declarations:\n token='token' and userid='userid'\nConfig files are read in the following order:\n 1. /etc/tgbpipe.conf\n 2. ~/.config/tgbpipe.conf\n 3. command line.\nValues loaded later overwrite previously read values."
	    exit 0
	    ;;

	-v|--version) #print version number
	    echo "tgbpipe $VERSION"
        exit 0
	    ;;

	-c|--config) #Read config file specified on command line
	    if [[ -r $2 ]]
	    then
            source "$2"
            shift
            shift
	    else
            echo "Error: Cannot read config file"
		exit 1
	    fi
	    ;;

	-t|--text) #Read text from file
	    if [[ -r $2 ]]
	    then
            text=$(cat $2)
            shift
            shift
	    else
            echo "Error: Cannot read text file"
            exit 1
	    fi
	    ;;		

	-u|--uid) #Read uid value from command line option
	    if [[ -n $2 ]]
	    then
            userid="$2"
            shift
            shift
	    else
            echo "Error: Cannot read user ID"
            exit 1
	    fi
	    ;;

	-k|--token) #Read token from command line
	    if [[ -n $2 ]]
	    then
            token="$2"
            shift
            shift
        else
            echo "Error: Cannot read token"
            exit 1
	    fi
	    ;;

	*)	#Unknown option on command line
	    echo "unknown command line argument"
	    exit 1
	    ;;

    esac
done

if [[ -z $text ]] #text not defined on command line
then
    text=$(cat) #Redirect stdin into variable
fi


if [[ -z $token || -z $userid ]] #Have all out values been defined?
then
    echo "Token or userid not specified."
    echo "Are the config files ok?"
    exit 1
fi

#send the data with curl
curl -s -F "chat_id=$userid" -F "text=$text" \
"https://api.telegram.org/bot$token/sendMessage" > /dev/null

