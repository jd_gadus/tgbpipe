# tgbpipe

## Pipe from stdin to a telegram bot.

Usage: tgbpipe [OPTION]
Telegram bot pipe. Send a text to a user via telegram bot

 -c, --config		Read config from file. If unspecified, read from files

 -t, --text file	Text to send. If unspecified, read from stdin

 -k, --token token	Bot token to use

 -u, --uid userid	User ID to send to. Only one ID currently supported

 -h, -?, --help		Show this help

 -v, --version		Show version

If multiple instances of -c, -k, -t and -u are specified, the last one in the
command line is used.

Config files are read with source and should countain two variable declarations:
 token='token' and userid='userid'
Config files are read in the following order:
 1. /etc/tgbpipe.conf
 2. ~/.config/tgbpipe.conf
 3. command line.
Values loaded later overwrite previously read values.
___
tgbpipe is written in bash and contains bashisms. It depends on `curl` to send network requests
